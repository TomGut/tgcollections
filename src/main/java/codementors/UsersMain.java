package codementors;

import codementors.Model.Gender;
import codementors.Model.User;
import codementors.Model.UserSort;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class UsersMain {
    public static void main(String args[]){
        //List type collection with generic User object created
        List<User> users = load("test.txt");
        System.out.println("users");
        print(users);

        //Set type collection with HashSet created
        Set usersSet = new HashSet<>();
        //adding all elements of users to usersSet
        usersSet.addAll(users);
        System.out.println("usersSet");
        print(usersSet);

        //Tree Set type collection created
        Set usersTreeSet = new TreeSet();
        //adding all elements of users to usersTreeSet
        usersTreeSet.addAll(users);
        System.out.println("usersTreeSet");
        print(usersTreeSet);

        //Map created
        Map<String, User> usersMap = loadDepartments("test2.txt");
        System.out.println("usersMap");
        print(usersMap);

        //Map created with List as param
        Map<String, List<User>> mapWithList = loadDepartments2("test2.txt");
        System.out.println("mapWithList");
        printMap(mapWithList);
        //Map with TreeMap
        Map<String, List<User>> mapWithList2 = new TreeMap<>();
        mapWithList2.putAll(mapWithList);
        System.out.println("mapWithList2");
        printMap(mapWithList2);

        //sorting users
        Collections.sort(users);
        System.out.println("users after sort");
        print(users);
        //sorting users with custom comparator in UserSort class
        Collections.sort(users, new UserSort());
        System.out.println("users after custom comparator sort by login");
        print(users);
    }

    //TODO ustalić sposób serwowania metody - jest konflikt z poprzednim map
    static void printMap(Map<String, List<User>> users){
        System.out.println(users);
    }

    //adding users form HDD file with HashMap and ArrayListas param instead of User Object param as before
    static Map<String, List<User>> loadDepartments2(String fileName){
        //creation of empty List and Map for switch
        List<User> listFruits = new ArrayList<>();
        List<User> listMeat = new ArrayList<>();
        List<User> listDairy = new ArrayList<>();
        List<User> listBread = new ArrayList<>();
        Map<String, List<User>> map = new HashMap<>();

        try(FileReader fr = new FileReader(fileName); BufferedReader br = new BufferedReader(fr)){
            //reading number of users and parsing for loop
            int index = Integer.parseInt(br.readLine());
            //null variables for loop
            String name;
            String surname;
            String login;
            Enum gender;
            String department;

            for(int i=0; i<index; i++ ){
                name = br.readLine();
                surname = br.readLine();
                login = br.readLine();
                gender = Gender.valueOf(br.readLine());
                //department used as Map collection key not as a User object variable in constructor
                department = br.readLine();
                //User class instance to be put in list
                User user = new User(login, name, surname, gender);

                switch (department){
                    case "fruits":{
                        listFruits.add(user);
                        map.put(department, listFruits);
                        break;
                    }case "meat": {
                        listMeat.add(user);
                        map.put(department, listMeat);
                        break;
                    }case "dairy": {
                        listDairy.add(user);
                        map.put(department, listDairy);
                        break;
                    }case "bread": {
                        listBread.add(user);
                        map.put(department, listBread);
                        break;
                    }
                }
            }
        }catch (IOException ex){
            System.err.println(ex);
        }
        return map;
    }

    //prints users from Map map
    static void print(Map<String, User> users){
        System.out.println(users);
    }

    //adding users from HDD file to Map and returns Map map
    static Map<String, User> loadDepartments(String fileName){
        //empty map for loop
        Map<String, User> map = new HashMap<>();
        //decorator usage upon fr
        try(FileReader fr = new FileReader(fileName); BufferedReader br = new BufferedReader(fr)){
            //reading number of users and parsing for loop
            int index = Integer.parseInt(br.readLine());
            //null variables for loop
            String name;
            String surname;
            String login;
            Enum gender;
            String department;

            for(int i=0; i<index; i++ ){
                name = br.readLine();
                surname = br.readLine();
                login = br.readLine();
                gender = Gender.valueOf(br.readLine());
                department = br.readLine();
                //User class instance
                User user = new User(login, name, surname, gender);
                //adding instance to map as an object with department as key
                map.put(department, user);
            }
        }catch (IOException ex){
            System.err.println(ex);
        }
        return map;
    }

    //loading users from txt file on HDD and returns List list
    private static List<User> load(String fileName){
        //empty list for loop
        List<User> list = new ArrayList<>();
        //decorator usage upon fr
        try(FileReader fr = new FileReader(fileName); BufferedReader br = new BufferedReader(fr)){
            //reading number of users and parsing for loop
            int index = Integer.parseInt(br.readLine());
            //null variables for loop
            String name;
            String surname;
            String login;
            Enum gender;

            for(int i=0; i<index; i++ ){
                name = br.readLine();
                surname = br.readLine();
                login = br.readLine();
                gender = Gender.valueOf(br.readLine());
                //User class instance
                User user = new User(login, name, surname, gender);
                //adding instance of User to list
                list.add(user);
            }
        }catch (IOException ex){
            System.err.println(ex);
        }
        return list;
    }

    //print can use any list not only User list now - earlier we used List<User> givenList
    private static void print(Collection givenCollection){
        //nie trzeba wywoływać to String gdyż sout drukuje string ktory juz w sobie ma toString + nadpisalismy ja w User
        System.out.println(givenCollection);
    }
}